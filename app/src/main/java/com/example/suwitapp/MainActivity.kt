package com.example.suwitapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random
import kotlin.random.nextInt

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var yourScore = 0
        var comScore = 0

        btnPaper.setOnClickListener{
            val versus = Versus(1)
            val result = versus.versus(versus.your, versus.listShuffle)
            changeImage(versus.your, versus.listShuffle)
            changeBg(versus.result)
            tv_results.text = versus.result

            when(versus.result){
                "Win" -> yourScore++
                "Lose" -> comScore++
            }
            your_score_result.text = yourScore.toString()
            com_score_result.text = comScore.toString()
        }

        btnRock.setOnClickListener {
            val versus = Versus(2)
            val result = versus.versus(versus.your, versus.listShuffle)
            changeImage(versus.your, versus.listShuffle)
            changeBg(versus.result)
            tv_results.text = versus.result
            when(versus.result){
                "Win" -> yourScore++
                "Lose" -> comScore++
            }
            your_score_result.text = yourScore.toString()
            com_score_result.text = comScore.toString()
        }

        btnScissor.setOnClickListener {
            val versus = Versus(3)
            val result = versus.versus(versus.your, versus.listShuffle)
            changeImage(versus.your, versus.listShuffle)
            changeBg(versus.result)
            tv_results.text = versus.result
            when(versus.result){
                "Win" -> yourScore++
                "Lose" -> comScore++
            }
            your_score_result.text = yourScore.toString()
            com_score_result.text = comScore.toString()
        }


        iv_refresh.setOnClickListener {
            your_Choose.setImageResource(android.R.color.transparent)
            com_Choose.setImageResource(android.R.color.transparent)
            tv_results.text = ""
            your_score_result.text = "0"
            com_score_result.text = "0"
            yourScore = 0
            comScore = 0
            bg_results.setImageResource(R.drawable.bg_win)
        }

    }


    fun changeImage(your:Int, com: Int) {
        when(your){
            1 -> your_Choose.setImageResource(R.drawable.btn_kertas)
            2 -> your_Choose.setImageResource(R.drawable.btn_batu)
            3 -> your_Choose.setImageResource(R.drawable.btn_gunting)
        }

        when(com){
            1 -> com_Choose.setImageResource(R.drawable.btn_kertas)
            2 -> com_Choose.setImageResource(R.drawable.btn_batu)
            3 -> com_Choose.setImageResource(R.drawable.btn_gunting)
        }
    }

    fun changeBg(result: String) {
        when(result){
            "Win" -> bg_results.setImageResource(R.drawable.bg_win)
            "Draw" -> bg_results.setImageResource(R.drawable.bg_draw)
            "Lose" -> bg_results.setImageResource(R.drawable.bg_lose_game)
        }
    }
}


class Versus(val your : Int){
        val listShuffle = Random.nextInt(1..3)
        var result = ""
        fun versus(your: Int, com: Int): String {
            if (your == com) {
                result = "Draw"
            } else {
                when (your) {
                    1 -> {
                        if (com == 2) {
                            result = "Win"
                        }
                        if (com == 3) {
                            result = "Lose"
                        }
                    }
                    2 -> {
                        if (com == 3) {
                            result = "Win"
                        }
                        if (com == 1) {
                            result = "Lose"
                        }
                    }
                    3 -> {
                        if (com == 1) {
                            result = "Win"
                        }
                        if (com == 2) {
                            result = "Lose"
                        }
                    }
                }
            }
            return result
        }
    }
